resource "helm_release" "kyverno_policies" {
  depends_on      = [helm_release.kyverno]
  chart           = "kyverno-policies"
  repository      = var.chart_repository
  name            = "${var.chart_name}-policies"
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history
  atomic          = var.helm_atomic
  timeout         = var.helm_timeout
}
